var $baseUrl = $("#base-url").html();
var viewCodeApp = angular.module('viewCodeApp',[]);

viewCodeApp.service('dataService', function($http) {
	this.getData = function(params, path, callbackFunc){
		$http({
			method: 'GET',
			url: $baseUrl + path,
			params: params
        	/// headers: {'Authorization': 'Token token=xxxxYYYYZzzz'}
        }).then(function(response) {
            callbackFunc(response);
        }, function(reason) {
            console.log({'Failed: ': reason, 'values': params, 'url': path});
        }, function(update) {
            console.log({'Got notification: ':  update, 'values': params, 'url': path});
        });
    };

    this.setData = function(params, path, callbackFunc){
        $.ajax({
            method: 'POST',
            url: $baseUrl + path,
            data: params
        }).then(function(response) {
            callbackFunc(response);
        }, function(reason) {
            console.log({'Failed: ': reason, 'values': params, 'url': path});
        }, function(update) {
            console.log({'Got notification: ':  update, 'values': params, 'url': path});
        });
    };

});

viewCodeApp.controller('viewCodeController', ['$scope', '$timeout', '$filter', 'dataService', function($scope, $timeout, $filter, dataService){
  console.log({"Controlador": "viewCodeController"});
  $scope.code = {
    title: "Javascript",
    type: "language-css", // -css, -php, -javascript - nginx
    content: "p { color: blue }"
  };
  
  /*var paramsView = {
		data: {elements: 'all'},
		url: 'get/dataview'
	};

	dataService.getData(paramsView.data, paramsView.url, function(dataResponse){
    console.log({"Respuesta code": dataResponse});
		if(dataResponse.data.response === true){
			$scope.code = dataResponse.data;
    };
	});*/
  
  
}]);