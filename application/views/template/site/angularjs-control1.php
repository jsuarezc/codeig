<div class="container">
		<a class="btn btn-default" href="<?php echo base_url("/curso/basicos/angularjs"); ?>" role="button">Aplicacion Basíca</a>
		<a class="btn btn-default" href="<?php echo base_url("/curso/basicos/angularjs-control1"); ?>" role="button">Controlador 1</a>
		<a class="btn btn-default" href="<?php echo base_url("/curso/basicos/angularjs-control2"); ?>" role="button">Controlador 2</a>
		<a class="btn btn-default" href="<?php echo base_url("/curso/basicos/angularjs-multiples"); ?>" role="button">Multiples controladores</a>
		<a class="btn btn-default" href="<?php echo base_url("/curso/basicos/angularjs-anidados"); ?>" role="button">Controladores Anidados</a>
		<br>
		<br>
		<div id="base-url" class="hide">
			<?php echo base_url(); ?>
		</div>
	</div>
<div class="container-fluid" id="viewApp" ng-app="viewCodeApp">
		<div class="col-md-4">
			<legend>
				<h2>
					HTML
				</h2>
			</legend>
			<pre class="language-markup line-numbers" data-line="7,15,17" style="height:400px;" data-src="<?php echo base_url("/curso/codes/angular-html-control1"); ?>"></pre>
		</div>
		<div class="col-md-4">
			<legend>
				<h2>
					JS
				</h2>
			</legend>
			<pre class="language-javascript line-numbers" data-line="5" style="height:400px;" data-src="<?php echo base_url("/curso/codes/angular-js-control1.js"); ?>"></pre>
		</div>
		<div class="col-md-4">
			<legend>
				<h2>
					CSS
				</h2>
			</legend>
			<pre class="language-css line-numbers" style="height:400px;" data-src="<?php echo base_url("/curso/codes/angular-css.css"); ?>"></pre>
		</div>
  	<div class="col-md-12"></div>
		<div class="col-md-12">
			<legend>
				<h2>
					Resultado
				</h2>
			</legend>
			<div class="thumbnail">
			 <iframe src="<?php echo base_url("/curso/resultado/angular-control1"); ?>" width="100%" frameborder="0" scrolling="auto" height="400"></iframe>
			 </div>
		</div>
	</div>