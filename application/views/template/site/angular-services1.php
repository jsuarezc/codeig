<div class="container">
		<a class="btn btn-primary" href="<?php echo base_url(); ?>" role="button">Inicio</a>
		<a class="btn btn-default" href="<?php echo base_url("/curso/basicos/angular-services1"); ?>" role="button">Formulario con selects dinamicos</a>
		<a class="btn btn-default" href="<?php echo base_url("/curso/basicos/angular-services2"); ?>" role="button">Formulario services</a>
		<br>
		<br>
		<div id="base-url" class="hide">
			<?php echo base_url(); ?>
		</div>
	</div>
<div class="container-fluid" id="viewApp" ng-app="viewCodeApp">
		<div class="col-md-4">
			<legend>
				<h2>
					HTML
				</h2>
			</legend>
			<pre class="language-markup line-numbers" data-line="" style="height:400px;" data-src="<?php echo base_url("/curso/resultado/angular-services1"); ?>"></pre>
		</div>
		<div class="col-md-4">
			<legend>
				<h2>
					JS
				</h2>
			</legend>
			<pre class="language-javascript line-numbers" style="height:400px;" data-src="<?php echo base_url("/assets/js/basicos/angular-services1.js"); ?>"></pre>
		</div>
		<div class="col-md-4">
			<legend>
				<h2>
					CSS
				</h2>
			</legend>
			<pre class="language-css line-numbers" style="height:400px;" data-src="<?php echo base_url("/curso/codes/angular-css.css"); ?>"></pre>
		</div>
  	<div class="col-md-12"></div>
		<div class="col-md-12">
			<legend>
				<h2>
					Resultado
				</h2>
			</legend>
			<div class="thumbnail">
			 <iframe src="<?php echo base_url("/curso/resultado/angular-services1"); ?>" width="100%" frameborder="0" scrolling="auto" height="400"></iframe>
			 </div>
		</div>
	</div>