<div class="container-fluid" id="viewApp" ng-app="viewCodeApp">
		<div class="hide">
				type: -css, -php, -javascript - nginx -markup
				jshtml, nginx.conf, javascript.js
			</div>
		<div class="col-md-4">
			<legend>
				<h2>
					HTML
				</h2>
			</legend>
			<pre class="language-markup line-numbers" data-line="7" style="height:400px;" data-src="<?php echo base_url("/curso/codes/jquery-html"); ?>"></pre>
		</div>
		<div class="col-md-4">
			<legend>
				<h2>
					JS
				</h2>
			</legend>
			<pre class="language-javascript line-numbers" style="height:400px;" data-src="<?php echo base_url("/curso/codes/jquery-js.js"); ?>"></pre>
		</div>
		<div class="col-md-4">
			<legend>
				<h2>
					CSS
				</h2>
			</legend>
			<pre class="language-css line-numbers" style="height:400px;" data-src="<?php echo base_url("/curso/codes/jquery-css.css"); ?>"></pre>
		</div>
  	<div class="col-md-12"></div>
		<div class="col-md-12">
			<legend>
				<h2>
					Resultado
				</h2>
			</legend>
			<div class="thumbnail">
			 <iframe src="<?php echo base_url("/curso/resultado/jquery"); ?>" width="100%" frameborder="0" scrolling="auto" height="400"></iframe>
			 </div>
		</div>
	</div>