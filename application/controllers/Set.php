<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set extends CI_Controller {

  function __construct()
	{
		parent::__construct();
	}
  
	public function index()
	{
    $data["data"]["view"] = "index";
		$this->load->view('/template/response/jsonfile', $data);
	}
  
  	
	public function formulario()
	{
		$params = $this->input->post(NULL, TRUE);
    $data['data']["view"] = "formulario";
		$data['data']["response"] = true;
		$data["data"]["departamentos"] = $params;
		$this->load->view('/template/response/jsonfile', $data);
	}	
}